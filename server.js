const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const session = require('express-session');
const app = express();
app.use(session({
	'secret':'secret'
}));

//----------------------------------------------> CONFIG
require('dotenv').config();
app.use(express.static(`${__dirname}/public`));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.set('view engine', 'pug');
app.set('views',__dirname +('/views'));


//----------------------------------------------> CREATION ROUTE
// app.get('/', function (req, res) {
// 	res.render("/post/connexion");
// });
//________________________________________________ROUTE DEPART
app.get("/", function(req, res) {
    res.render("posts/register")
});
//________________________________________________ROUTE INSCRIPTION

app.post('/posts/register', function (req, res) {
	let user_name = req.body.user_name;
	let user_password = req.body.user_password;
	database.query(`INSERT INTO users(user_name, user_password) VALUES("${user_name}","${user_password}");`, (err, result, fields) => {
		if(err){
			console.log(err)
		}
		res.redirect('/register/connexion');
	})
});
app.get("/register/connexion", function(req, res) {
    res.render("posts/login")
});
//________________________________________________ROUTE CONNEXION

app.post('/posts/register/login', function (req, res) {
	let user_name = req.body.user_name;
	let user_password = req.body.user_password;
	database.query(`SELECT * FROM users WHERE user_name ="${user_name}";`, (err, result, fields) => {
		console.log(result)
		if(result.length < 1){
			res.send("Pas encore inscrit")
		}
		else if(user_name === result[0].user_name){
			req.session.login = result[0].user_name
			console.log(req.session.login)
		}
		if(user_password === result[0].user_password){

			res.redirect('/showDefi');
		}
	})
});
app.get("connexion/home", function(req, res) {
	res.render("posts/index")
});	

//_______________________________________afficher les defis
app.get('/showDefi', function(req, res){
	database.query('SELECT title, content FROM defis;', function(err, result, fields){
		if(err){
			console.log(err)
			return
		}
		res.render('posts/index',{results : result})
	
	})
});
app.get('/posts/creat', (req, res) => res.render('posts/creat'));
//_______________________________________crer un defis

app.post('/posts/newdefi', function (req, res){
	
	database.query(`INSERT INTO defis(title, content) VALUES("${req.body.title}","${req.body.content}");`,function (err,result, fields){
		console.log(" New challenge send to database");
	if (err){
		console.log(err);
	}
	// console.log(result)
	res.redirect("/showDefi");
	// console.log(results);
	})
});

//----------------------------------------------> CONNEXION DATABASE
const database = mysql.createConnection({
    host : process.env.bd_host,
    user :  process.env.bd_user,
    password : process.env.bd_password,
    database :  process.env.bd_database,
});
database.connect((err) => {
    if(err){
        console.log(err);
        return;
    }
    console.log('DATABASE connected ! ')
});
//----------------------------------------------> SERVEUR
app.listen(process.env.port, () => console.log(`Server en route ! port : ${process.env.port}`));